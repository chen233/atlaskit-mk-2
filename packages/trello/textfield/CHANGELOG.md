# @nachos/textfield

## 0.1.2

### Patch Changes

- [patch][6742fbf2cc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6742fbf2cc):

  bugfix, fixes missing version.json file

## 0.1.1

### Patch Changes

- [patch][18dfac7332](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/18dfac7332):

  In this PR, we are:

  - Re-introducing dist build folders
  - Adding back cjs
  - Replacing es5 by cjs and es2015 by esm
  - Creating folders at the root for entry-points
  - Removing the generation of the entry-points at the root
    Please see this [ticket](https://product-fabric.atlassian.net/browse/BUILDTOOLS-118) or this [page](https://hello.atlassian.net/wiki/spaces/FED/pages/452325500/Finishing+Atlaskit+multiple+entry+points) for further details

## 0.1.0

- [minor][7c17b35107](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7c17b35107):

  - Updates react and react-dom peer dependencies to react@^16.8.0 and react-dom@^16.8.0. To use this package, please ensure you use at least this version of react and react-dom.

## 0.0.7

- Updated dependencies [6cdf11238d](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6cdf11238d):
  - @atlaskit/textfield@1.0.0

## 0.0.6

- [patch][0a4ccaafae](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0a4ccaafae):

  - Bump tslib

## 0.0.5

- [patch][a136a8c8be](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a136a8c8be):

  - Releasing nachos textfield component.

## 0.0.4

- Updated dependencies [8eff47cacb](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8eff47cacb):
  - @atlaskit/textfield@0.4.0

## 0.0.3

- [patch][697044626f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/697044626f):

  - Bumping to follow all the other bumping in AK

## 0.0.2

- [patch][03ff6e99e2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/03ff6e99e2):

  - First release of Nachos textfield! 🎉 This is the React implementation of a textfield from the [Nachos design system](https://design.trello.com/).
