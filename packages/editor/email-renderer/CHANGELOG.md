# @atlaskit/email-renderer

## 2.9.0

### Minor Changes

- [minor][435258881c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/435258881c):

  CS-1238 Media honor width and flow settings

## 2.8.0

### Minor Changes

- [minor][4c3772ce61](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4c3772ce61):

  CS-1238 Added generic icon for media attachments

## 2.7.1

### Patch Changes

- [patch][59fb844cd5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59fb844cd5):

  CS-1184 Email renderer - prevent tables from flowing outside container

## 2.7.0

### Minor Changes

- [minor][5b89d23a43](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5b89d23a43):

  CS-1184 Email renderer icons compressed, rendered diff looks better for some nodes

## 2.6.0

### Minor Changes

- [minor][d08834952b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d08834952b):

  CS-1184 CSS prefix shortened

## 2.5.0

### Minor Changes

- [minor][8cd4402937](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8cd4402937):

  CSS corrections

## 2.4.0

### Minor Changes

- [minor][0718ea79a0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0718ea79a0):

  Email renderer notification distributor integration

## 2.3.0

### Minor Changes

- [minor][22af1d9ddd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/22af1d9ddd):

  FS-4032 - Remove background styling from actions and decisions in email. Unable to support hover.

## 2.2.0

### Minor Changes

- [minor][c617f954b7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c617f954b7):

  Extracted CSS for last set of 4 ADF nodes

## 2.1.0

### Minor Changes

- [minor][86218aa155](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/86218aa155):

  Email renderer: Extracted CSS for 8 more nodes

## 2.0.0

### Major Changes

- [major][38d11825f2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/38d11825f2):

  Renderer does not inline CSS anymore, but can be turned on by a flag for testing purposes

## 1.4.0

### Minor Changes

- [minor][60f541121b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/60f541121b):

  Added buildstep into atlaskit pipeline

## 1.3.0

### Minor Changes

- [minor][34c6df4fb8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/34c6df4fb8):

  adf-schema has been extended with one missing color, email-renderer now bundles up styles into .css file

## 1.2.0

### Minor Changes

- [minor][b5c75d12d5](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b5c75d12d5):

  adds support for embedded images in email renderer

## 1.1.1

### Patch Changes

- [patch][fa7d25c521](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/fa7d25c521):

  Email renderer es5 tsconfig file tweak

## 1.1.0

### Minor Changes

- [minor][59da918bba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/59da918bba):

  Email renderer builds to es5

## 1.0.0

### Major Changes

- [major][ff85c1c706](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ff85c1c706):

  Extracted email renderer outside react renderer
