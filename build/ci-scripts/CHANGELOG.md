# @atlaskit/ci-scripts

## 1.0.2
### Patch Changes

- [patch] [18dfac7332](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/18dfac7332):

  In this PR, we are:
  * Re-introducing dist build folders
  * Adding back cjs
  * Replacing es5 by cjs and es2015 by esm
  * Creating folders at the root for entry-points
  * Removing the generation of the entry-points at the root
  Please see this [ticket](https://product-fabric.atlassian.net/browse/BUILDTOOLS-118) or this [page](https://hello.atlassian.net/wiki/spaces/FED/pages/452325500/Finishing+Atlaskit+multiple+entry+points) for further details
